%define EXIT 60
%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define SYMB_ASCII 48
%define MINUS 45
%define MAX 58
%define MIN 47
%define NEW_STRING 10

section .text

global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global print_error
global print 
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    mov     rax, EXIT
    syscall

print_error:
    mov rcx, STDERR         
    jmp print


print:
    push rcx
    call string_length
    pop rcx
    mov rdx, rax
    mov rsi, rdi
    mov rdi, rcx
    mov rax, STDOUT  ; номер системного вызова
    syscall
    ret


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor   rax, rax
.loop:
    cmp  byte [rdi + rax], STDIN
    je   .end
    inc  rax
    jmp .loop
.end:
    ret
 
; Принимает код символа и выводит его в stdout
print_char:
    push    rdi
    mov     rsi, rsp
    mov     rax, STDOUT
    mov     rdi, STDOUT
    mov     rdx, STDOUT
    syscall
    pop     rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov     r9, rdi
    mov     rax, STDOUT
    mov     rdi, STDOUT
.val:
    cmp     byte [r9], STDIN
    je      .end
    mov     rsi, r9
    mov     rdx, STDOUT
    syscall
    inc     r9
    jmp     .val
.end:
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rsp, NEW_STRING

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    xor rcx, rcx
    dec rsp
    mov [rsp], al
    mov r10, 10
    mov rax, rdi
.number:
    xor rdx, rdx
    div r10
    add dl, SYMB_ASCII
    dec rsp
    mov [rsp], dl
    inc rcx
    test rax, rax
    jnz .number
.print:
    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    add rsp, rcx
    inc rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    push rdi
    cmp rdi, STDIN
    jge .plus_number
    mov r9, rdi
    mov rdi, MINUS
    push r9
    call print_char
    mov rdi, r9
    neg rdi
    pop r9
.plus_number:
    call print_uint
.end:
    pop rdi
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r10
    push rdi
    push rsi
    call string_length
    mov r10, rax
    mov rdi, rsi
    call string_length
    mov r11, rax
    pop rsi
    pop rdi
    cmp r10, r11
    jne .not_equal
.equal_len:
    cmp r10, STDIN
    je .end
    cmpsb
    jne .not_equal
    dec r10
    loop .equal_len
.not_equal:
    pop r10
    xor rax, rax
    ret
.end:
    pop r10
    mov rax, STDOUT
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push STDIN				
	mov rax, STDIN			
    mov rdx, STDOUT			
    mov rdi, STDIN			
    mov rsi, rsp			
    syscall
    pop rax		
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push r12
	push r13
	push r14			
	mov r12, rdi			
	mov r13, rsi			
	dec r13			
	mov r14, STDIN		
.check_first:
	call read_char			
	cmp rax, 0x0			
	je .exit			
	cmp rax, 0x20			
	je .check_first		
	cmp rax, 0x9			
	je .check_first		
	cmp rax, 0xA			
	je .check_first		
.loop:
	cmp rax, 0x0			
	je .exit			
	cmp rax, 0x20			
	je .exit			
	cmp rax, 0x9		
	je .exit			
	cmp rax, 0xA			
	je .exit			
	mov [r12+r14], al		
	inc r14			
	cmp r14, r13			
	jg .fail			
	call read_char			
	jmp .loop			
.fail:
	pop r14
	pop r13
	pop r12
	mov rax, STDIN
	ret
.exit:
	mov byte[r12+r14], STDIN			
	mov rax, r12			
	mov rdx, r14			
	pop r14				
	pop r13
	pop r12			
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    xor rax, rax
    xor rdx, rdx
    mov r10, 10
.parse:
    cmp byte [rdi], MAX
    jge .end
    cmp byte [rdi], MIN
    jle .end
    push rdx
    mul r10
    pop rdx
    mov bl, byte [rdi]
    add al, bl
    sub al, SYMB_ASCII
    inc rdi
    inc rdx
    jmp .parse
.end:
    pop rbx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], MINUS
    jne .positive
    mov r9, rdi
    inc r9
    mov rdi, r9
    call parse_uint
    cmp rdx, STDIN
    je .end
    neg rax
    inc rdx
    jmp .end
.positive:
    call parse_uint
.end:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.str:
    cmp rax, rdx
    je .cannot
    mov bl, byte [rdi + rax]
    mov byte [rsi + rax], bl
    cmp byte [rsi + rax], STDIN
    je .end
    inc rax
    jmp .str
.cannot:
    xor rax, rax
.end:
    ret
